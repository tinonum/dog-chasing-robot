# Dog Catcher

This program was created for JETANK robot to catch dogs. The camera on the JETANK robot is used to detect dogs, using YOLO real time object detection. When a dog is detected, the robot moves forward, or turns towards the dog, depending on the location of the dog bounding box center X coordinate.

This program is intended to be ran inside a jupyter notebook interface of the JETANK robot.

## Video demo

![](dog_demo.mp4)

## Requirements

Running this program requires a built JETANK robot kit. 

Following repository downloaded into a folder named 'yolo' inside the project folder
https://github.com/pjreddie/darknet

Trained model YOLOv3-tiny inside the project folder from
https://pjreddie.com/darknet/yolo/


## Authors

Juho Kangas

Tino Nummela
